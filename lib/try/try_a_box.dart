import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_puzzle/kotak_cubit.dart';

class ABoxWidget extends StatefulWidget {
  const ABoxWidget({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ABoxWidget> createState() => _ABoxWidgetState();
}

class _ABoxWidgetState extends State<ABoxWidget> {
  int _currentPosX = 0;
  int _currentPosY = 0;

  static const minPosX = 0;
  static const maxPosX = 3;

  static const minPosY = 0;
  static const maxPosY = 3;

  void _moveStepX(int increment) {
    setState(() {
      var oldX = _currentPosX;
      var newX = oldX + increment;
      if (newX > maxPosX) {
        newX = maxPosX;
      }
      if (newX < 0) {
        newX = 0;
      }
      _currentPosX = newX;
    });
  }

  void _moveStepY(int increment) {
    setState(() {
      var oldY = _currentPosY;
      var newY = oldY + increment;
      if (newY > maxPosY) {
        newY = maxPosY;
      }
      if (newY < 0) {
        newY = 0;
      }
      _currentPosY = newY;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 144,
      height: 144,
      child: SizedBox.square(
        dimension: 144,
        child: Container(
          width: 144,
          height: 144,
          child: Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Text('(' +
                        _currentPosX.toString() +
                        ',' +
                        _currentPosY.toString() +
                        ')'),
                    GestureDetector(
                      onTap: () {
                        _moveStepY(-1);
                      },
                      child: Text('UP'),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              _moveStepX(-1);
                            },
                            child: Text('LEFT'),
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              _moveStepX(1);
                            },
                            child: Text('RIGHT'),
                          ),
                        ),
                      ],
                    ),
                    GestureDetector(
                      onTap: () {
                        _moveStepY(1);
                      },
                      child: Text('DOWN'),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
