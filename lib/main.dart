// ignore_for_file: avoid_print
// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_literals_to_create_immutables

import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_puzzle/kotak_cubit.dart';
import 'package:flutter_puzzle/wt_kotak_container.dart';

void main() {
  runApp(const KotakApp());
}

class KotakApp extends StatelessWidget {
  const KotakApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'EP Flutter Puzzle',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const KotakHomePage(title: 'EP Flutter Puzzle'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class KotakHomePage extends StatefulWidget {
  const KotakHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<KotakHomePage> createState() => _KotakHomePageState();
}

class _KotakHomePageState extends State<KotakHomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (BuildContext context) {
          return KotakCubit(KotakCubitStateInit());
        }),
      ],
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: OrientationBuilder(builder: (context, orientation) {
          return Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                generateTopWidget(),
                generateBoardWidget(),
                generateButtomWidget(context),
              ],
            ),
          );
        }),
        // floatingActionButton: FloatingActionButton(
        //   onPressed: _incrementCounter,
        //   tooltip: 'Increment',
        //   child: const Icon(Icons.add),
        // ),
      ),
    );
  }

  Expanded generateBoardWidget() {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: AspectRatio(
            aspectRatio: 1 / 1,
            child: Container(
              padding: const EdgeInsets.all(4.0),
              decoration: BoxDecoration(
                color: Colors.blue.shade50,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(
                  Radius.circular(8.0),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: BlocListener<KotakCubit, KotakCubitState>(
                  listener: (context, state) {
                    if (state is KotakCubitStateNewGame) {
                      print('KotakCubitStateNewGame');
                      setState(() {});
                    }

                    if (state is KotakCubitStateSolved) {
                      _showSlovedDialog(
                          context, state.textTotalTime, state.textTotalMove);
                    }
                  },
                  child: LayoutBuilder(
                    builder: (context, constraints) {
                      print('LayoutBuilder');
                      final configMaxWidth = constraints.maxWidth;

                      List<Widget> listChild = [];
                      int i = 0;

                      var _listPoint = context.read<KotakCubit>().listPoint();
                      for (var itemPoint in _listPoint) {
                        listChild.add(
                          WtKotakContainer(
                            configTitle: 'title.' + i.toString(),
                            configMaxWidth: configMaxWidth,
                            configInitialPos: itemPoint,
                            configOnClick: (oldPos, newPos) {
                              //
                            },
                          ),
                        );

                        i++;
                      }

                      return Stack(
                        children: listChild,
                      );
                    },
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Row generateTopWidget() {
    return Row(
      children: [
        Expanded(
          child: Container(
            padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(
                    Icons.timer_outlined,
                    color: Colors.indigo,
                  ),
                  SizedBox(width: 8),
                  BlocBuilder<KotakCubit, KotakCubitState>(
                    builder: (context, state) {
                      var data = context.read<KotakCubit>().getLastStopwatch();
                      return Text(
                        data,
                        style: TextStyle(
                          color: Colors.indigo.shade700,
                          fontSize: 14,
                        ),
                      );
                    },
                    buildWhen: (context, state) {
                      return state is KotakCubitStateStopwatch;
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          child: Container(
            padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
            child: Row(
              children: [
                Icon(
                  Icons.sync_alt,
                  color: Colors.indigo,
                ),
                SizedBox(width: 8),
                BlocBuilder<KotakCubit, KotakCubitState>(
                  builder: (context, state) {
                    var data = context.read<KotakCubit>().getLastCountMove();
                    return Text(
                      data,
                      style: TextStyle(
                        color: Colors.indigo.shade700,
                        fontSize: 14,
                      ),
                    );
                  },
                  buildWhen: (context, state) {
                    return state is KotakCubitStateCountMove;
                  },
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
            child: Row(
              children: [
                Icon(
                  Icons.swipe,
                  color: Colors.indigo,
                ),
                SizedBox(width: 8),
                BlocBuilder<KotakCubit, KotakCubitState>(
                  builder: (context, state) {
                    var data = context.read<KotakCubit>().getLastPracticeMode();
                    return Text(
                      data,
                      style: TextStyle(
                        color: Colors.indigo.shade700,
                        fontSize: 14,
                      ),
                    );
                  },
                  buildWhen: (context, state) {
                    return state is KotakCubitStatePracticeMode;
                  },
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Row generateButtomWidget(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: GestureDetector(
            onTap: () {
              context.read<KotakCubit>().btnReset();
            },
            child: Container(
              padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
              child: Row(
                children: [
                  Icon(
                    Icons.dashboard_customize,
                    color: Colors.indigo,
                  ),
                  SizedBox(width: 8),
                  Text(
                    'Reset',
                    style: TextStyle(
                      color: Colors.indigo.shade700,
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          child: GestureDetector(
            onTap: () {
              context.read<KotakCubit>().btnShuffleTiles();
            },
            child: Container(
              padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
              child: Row(
                children: [
                  Icon(
                    Icons.shuffle_rounded,
                    color: Colors.indigo,
                  ),
                  SizedBox(width: 8),
                  Text(
                    'Shuffle',
                    style: TextStyle(
                      color: Colors.indigo.shade700,
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _showSlovedDialog(
    BuildContext context,
    String textTotalTime,
    String textTotalMove,
  ) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text("Puzzle Solved"),
          content: Text("$textTotalMove Move\nTime $textTotalTime"),
          actions: [
            CupertinoDialogAction(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
          ],
        );
      },
    );
  }
}
