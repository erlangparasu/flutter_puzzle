// ignore_for_file: avoid_print

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_puzzle/kotak_cubit.dart';
import 'package:flutter_puzzle/wt_kotak.dart';

typedef WtKotakContainerOnClick = void Function(
    Point<double> oldPoint, Point<double> newPoint);

class WtKotakContainer extends StatefulWidget {
  const WtKotakContainer({
    Key? key,
    required this.configTitle,
    required this.configMaxWidth,
    required this.configInitialPos,
    required this.configOnClick,
  }) : super(key: key);

  final String configTitle;
  final double configMaxWidth;
  final Point<double> configInitialPos;
  final WtKotakContainerOnClick configOnClick;

  @override
  State<WtKotakContainer> createState() => _WtKotakContainerState();
}

class _WtKotakContainerState extends State<WtKotakContainer> {
  double _configTop = 0;
  double _configLeft = 0;

  Curve _animationCurve = Curves.fastOutSlowIn;
  Duration _animationDuration = const Duration(milliseconds: 350);

  @override
  void initState() {
    final double pieceWidth = widget.configMaxWidth / 4;
    _configTop = pieceWidth * widget.configInitialPos.y;
    _configLeft = pieceWidth * widget.configInitialPos.x;

    super.initState();
  }

  final GlobalKey _key1 = GlobalKey();

  @override
  Widget build(BuildContext context) {
    final double pieceWidth = widget.configMaxWidth / 4;
    return BlocListener<KotakCubit, KotakCubitState>(
      listener: (context, state) {
        if (state is KotakCubitStateMove) {
          if (state.boxId == widget.configTitle) {
            // print(state.boxId);
            // print(state.position);

            setState(() {
              if (state.slowAnimate) {
                _animationCurve = Curves.fastOutSlowIn;
                _animationDuration = const Duration(milliseconds: 100);
              } else {
                _animationCurve = Curves.fastOutSlowIn;
                _animationDuration = const Duration(milliseconds: 50);
              }

              final Point<double>? position = state.uiPosition;
              if (null != position) {
                _configTop = position.y;
                _configLeft = position.x;
              }
            });
          }
        }
      },
      child: Container(
        // color: Colors.yellow,
        width: widget.configMaxWidth,
        height: widget.configMaxWidth,
        child: SizedBox.square(
          dimension: widget.configMaxWidth,
          child: Container(
            width: widget.configMaxWidth,
            height: widget.configMaxWidth,
            child: Stack(
              children: [
                AnimatedPositioned(
                  curve: _animationCurve,
                  duration: _animationDuration,
                  width: pieceWidth,
                  height: pieceWidth,
                  top: _configTop,
                  left: _configLeft,
                  child: Builder(
                    builder: (context) {
                      return WtKotak(
                        key: _key1,
                        configTitle: widget.configTitle,
                        configInitialPos: widget.configInitialPos,
                        configMaxWidth: pieceWidth,
                        configOnClick: (oldPos, newPos) {
                          //
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
