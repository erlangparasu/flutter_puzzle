// ignore_for_file: avoid_print, prefer_const_constructors

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_puzzle/kotak_cubit.dart';

typedef WtKotakOnClick = void Function(
    Point<double> oldPoint, Point<double> newPoint);

class WtKotak extends StatefulWidget {
  const WtKotak({
    Key? key,
    required this.configTitle,
    required this.configMaxWidth,
    required this.configOnClick,
    required this.configInitialPos,
  }) : super(key: key);

  final String configTitle;
  final double configMaxWidth;
  final WtKotakOnClick configOnClick;
  final Point<double> configInitialPos;

  @override
  State<WtKotak> createState() => _WtKotakState();
}

class _WtKotakState extends State<WtKotak> {
  double _currentPosX = 0;
  double _currentPosY = 0;

  @override
  void initState() {
    _currentPosX = widget.configInitialPos.x;
    _currentPosY = widget.configInitialPos.y;

    context.read<KotakCubit>().event_initStateKotak(
          widget.configMaxWidth,
        );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final key1 = widget.key as GlobalKey;

    return BlocListener<KotakCubit, KotakCubitState>(
      listener: (BuildContext context, KotakCubitState state) {
        if (state is KotakCubitStateMove) {
          if (state.boxId == widget.configTitle) {
            // print(state.boxId);
            // print(state.point);

            setState(() {
              _currentPosX = state.point.x;
              _currentPosY = state.point.y;
            });
          }
        }
      },
      child: GestureDetector(
        /// TAP
        onTap: () {
          print('onTap');
          context.read<KotakCubit>().event_onTap(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                widget.configMaxWidth,
              );
        },
        onTapDown: (TapDownDetails details) {
          print('onTapDown x=' + details.localPosition.dx.toString());
          context.read<KotakCubit>().event_onTapDown(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                details.localPosition,
                details.globalPosition,
                widget.configMaxWidth,
              );
        },
        onTapCancel: () {
          print('onTapCancel');
          context.read<KotakCubit>().event_onTapCancel(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                widget.configMaxWidth,
              );
        },
        onTapUp: (TapUpDetails details) {
          print('onTapUp');
          context.read<KotakCubit>().event_onTapUp(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                details.localPosition,
                details.globalPosition,
                widget.configMaxWidth,
              );
        },

        /// HORIZONTAL
        onHorizontalDragDown: (DragDownDetails details) {
          print('onHorizontalDragDown');

          context.read<KotakCubit>().event_onHorizontalDragDown(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                details.localPosition,
                details.globalPosition,
                widget.configMaxWidth,
              );
        },
        onHorizontalDragStart: (DragStartDetails details) {
          print('onHorizontalDragStart');

          context.read<KotakCubit>().event_onHorizontalDragStart(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                details.localPosition,
                details.globalPosition,
                widget.configMaxWidth,
              );
        },
        onHorizontalDragCancel: () {
          print('onHorizontalDragCancel');

          context.read<KotakCubit>().event_onHorizontalDragCancel(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                widget.configMaxWidth,
              );
        },
        onHorizontalDragUpdate: (DragUpdateDetails details) {
          // print('onHorizontalDragUpdate x: ' +
          //     details.localPosition.dx.toString());

          context.read<KotakCubit>().event_onHorizontalDragUpdate(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                details.localPosition,
                details.globalPosition,
                widget.configMaxWidth,
              );
        },
        onHorizontalDragEnd: (DragEndDetails details) {
          print('onHorizontalDragEnd');

          context.read<KotakCubit>().event_onHorizontalDragEnd(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                widget.configMaxWidth,
              );
        },

        /// VERTICAL
        onVerticalDragDown: (DragDownDetails details) {
          print('onVerticalDragDown');

          context.read<KotakCubit>().event_onVerticalDragDown(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                details.localPosition,
                details.globalPosition,
                widget.configMaxWidth,
              );
        },
        onVerticalDragStart: (DragStartDetails details) {
          print('onVerticalDragStart');

          context.read<KotakCubit>().event_onVerticalDragStart(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                details.localPosition,
                details.globalPosition,
                widget.configMaxWidth,
              );
        },
        onVerticalDragCancel: () {
          print('onVerticalDragCancel');

          context.read<KotakCubit>().event_onVerticalDragCancel(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                widget.configMaxWidth,
              );
        },
        onVerticalDragUpdate: (DragUpdateDetails details) {
          // print('onVerticalDragUpdate x: ' +
          //     details.localPosition.dx.toString());

          context.read<KotakCubit>().event_onVerticalDragUpdate(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                details.localPosition,
                details.globalPosition,
                widget.configMaxWidth,
              );
        },
        onVerticalDragEnd: (DragEndDetails details) {
          print('onVerticalDragEnd');

          context.read<KotakCubit>().event_onVerticalDragEnd(
                widget.configTitle,
                Point(_currentPosX, _currentPosY),
                key1,
                widget.configMaxWidth,
              );
        },
        child: FittedBox(
          fit: BoxFit.scaleDown,
          child: Container(
            margin: const EdgeInsets.all(4.0),
            decoration: BoxDecoration(
              color: Colors.indigo.shade700,
              shape: BoxShape.rectangle,
              // border: Border.all(
              //   width: 2,
              //   color: Colors.blue.shade900,
              // ),
              borderRadius: const BorderRadius.all(
                Radius.circular(6.0),
              ),
            ),
            width: widget.configMaxWidth,
            height: widget.configMaxWidth,
            child: SizedBox.square(
              dimension: widget.configMaxWidth,
              child: Container(
                width: widget.configMaxWidth,
                height: widget.configMaxWidth,
                child: generateContentWidget1(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget generateContentWidget1() {
    var angka = 1 + int.parse(widget.configTitle.split('.').last.toString());
    return Center(
      child: Text(
        angka.toString(),
        style: TextStyle(
          color: Colors.white,
          fontSize: 28,
        ),
      ),
    );
  }

  Row generateContentWidget() {
    return Row(
      children: [
        Expanded(
          child: Column(
            children: [
              Text(widget.configTitle),
              Builder(
                builder: (context) {
                  var msg = '(' +
                      _currentPosX.toString() +
                      ',' +
                      _currentPosY.toString() +
                      ')';
                  return Text(msg);
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}
