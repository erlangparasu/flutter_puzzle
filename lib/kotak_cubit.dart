// ignore_for_file: avoid_print

import 'dart:async';
import 'dart:collection';
import 'dart:math';
import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_puzzle/custom_convert.dart';

abstract class KotakCubitState {}

class KotakCubitStateInit extends KotakCubitState {}

class KotakCubitStateNewGame extends KotakCubitState {
  KotakCubitStateNewGame();
}

class KotakCubitStateMove extends KotakCubitState {
  KotakCubitStateMove({
    required this.boxId,
    required this.point,
    required this.direction,
    required this.uiPosition,
    required this.slowAnimate,
  });

  final String boxId;
  final Point<double> point;
  final String direction;
  final Point<double>? uiPosition;
  final bool slowAnimate;
}

class KotakCubitStateSolved extends KotakCubitState {
  KotakCubitStateSolved({
    required this.textTotalTime,
    required this.textTotalMove,
  });

  final String textTotalTime;
  final String textTotalMove;
}

class KotakCubitStatePracticeMode extends KotakCubitState {
  KotakCubitStatePracticeMode({
    required this.text,
  });

  final String text;
}

class KotakCubitStateStopwatch extends KotakCubitState {
  KotakCubitStateStopwatch({
    required this.text,
  });

  final String text;
}

class KotakCubitStateCountMove extends KotakCubitState {
  KotakCubitStateCountMove({
    required this.text,
  });

  final String text;
}

class KotakCubit extends Cubit<KotakCubitState> {
  KotakCubitStateStopwatch _sttStopwatch =
      KotakCubitStateStopwatch(text: '00:00:00');

  KotakCubitStatePracticeMode _sttPracticeMode =
      KotakCubitStatePracticeMode(text: 'Practice');

  KotakCubitStateCountMove _sttMoveCount =
      KotakCubitStateCountMove(text: 0.toString());

  static const int configShuffleMoveCount = 123;

  Timer? _timerShuffle;
  Timer? _timerStartStopwatch;
  Timer? _timerTapWatcher;

  int _moveCount = 0;

  bool _isStopwatchEnabled = true;
  bool _isInPracticeMode = true;
  bool _isShuffling = false;

  KotakCubit(KotakCubitState initialState) : super(initialState) {
    _init();
  }

  /// Fields -------------------------------------------------------------------------

  Point<double> _lastRemovedPoint = const Point(3, 3);
  double? _configTileMaxWidth;

  final List<Point<double>> _listPoint = [];
  final _listBookedPoint = HashSet<String>();

  final _mappedPointAndId = HashMap<String, String?>();
  final _mappedCorrectIdPoint = HashMap<String, String>();

  final List<Point<double>> _listValidPoint = [];

  double? _tmpInitialDragX;
  double? _tmpInitialDragY;

  bool _isNeedClosestAvailablePoint = false;

  Offset? _updateDragLocalPositionHorl;
  Offset? _updateDragGlobalPositionHorl;
  Offset? _firstDragLocalPositionHorl;
  Offset? _firstDragGlobalPositionHorl;

  Offset? _updateDragLocalPositionVerl;
  Offset? _updateDragGlobalPositionVerl;
  Offset? _firstDragLocalPositionVerl;
  Offset? _firstDragGlobalPositionVerl;

  String selectedDrag = '';

  int _shuffleCounterTick = 0;

  final _stopwatch = Stopwatch();

  /// Override -------------------------------------------------------------------------

  @override
  void onChange(Change<KotakCubitState> change) {
    // print(change);
    super.onChange(change);
  }

  @override
  void onError(Object error, StackTrace stackTrace) {
    print('$error, $stackTrace');
    super.onError(error, stackTrace);
  }

  /// Box Clicked -------------------------------------------------------------------------

  Future<void> event_onTap(
    final String boxId,
    final Point<double> pointOld,
    final double widthMax,
  ) async {
    print('event_onTap');

    print('_lastRemovedPoint: ' + _lastRemovedPoint.toString());

    final heightMax = widthMax;
    _isNeedClosestAvailablePoint = false;

    // Find empty slot
    Point<double>? pointFou;

    final Point<double> pointT = pointOld + const Point<double>(0, -1);
    final Point<double> pointB = pointOld + const Point<double>(0, 1);
    final Point<double> pointL = pointOld + const Point<double>(-1, 0);
    final Point<double> pointR = pointOld + const Point<double>(1, 0);

    String direction = "";

    if (_listValidPoint.contains(pointT)) {
      if (!_listBookedPoint.contains(pointT.toString())) {
        pointFou = pointT;
        direction = "T";
        print('found slot');
        print(pointFou);
      }
    }
    if (_listValidPoint.contains(pointB)) {
      if (!_listBookedPoint.contains(pointB.toString())) {
        pointFou = pointB;
        direction = "B";
        print('found slot');
        print(pointFou);
      }
    }
    if (_listValidPoint.contains(pointL)) {
      if (!_listBookedPoint.contains(pointL.toString())) {
        pointFou = pointL;
        direction = "L";
        print('found slot');
        print(pointFou);
      }
    }
    if (_listValidPoint.contains(pointR)) {
      if (!_listBookedPoint.contains(pointR.toString())) {
        pointFou = pointR;
        direction = "R";
        print('found slot');
        print(pointFou);
      }
    }

    if (null != pointFou) {
      final double xFou = pointFou.x * widthMax;
      final double yFou = pointFou.y * heightMax;

      final double newBoxX = xFou;
      final double newBoxY = yFou;
      final Point<double> uiPosition = Point(newBoxX, newBoxY);

      _listBookedPoint.remove(pointOld.toString());
      _listBookedPoint.add(pointFou.toString());
      _lastRemovedPoint = Point(pointOld.x, pointOld.y);

      _mappedPointAndId[pointOld.toString()] = null;
      _mappedPointAndId[pointFou.toString()] = boxId;

      if (pointOld != pointFou) {
        _onTileWillMovedToNewPoint();
      }

      print('_listBookedPoint remove: ' + pointOld.toString());
      print('_listBookedPoint add: ' + pointFou.toString());
      print('_lastRemovedPoint: ' + _lastRemovedPoint.toString());

      emit(KotakCubitStateMove(
        boxId: boxId,
        point: pointFou,
        direction: direction,
        uiPosition: uiPosition,
        slowAnimate: true,
      ));
    }
  }

  /// Horizontal Drag -------------------------------------------------------------------------

  Future<void> event_onHorizontalDragUpdate(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey key1,
    final Offset dragLocalPosition,
    final Offset dragGlobalPosition,
    final double widthMax,
  ) async {
    _isNeedClosestAvailablePoint = true;
    _updateDragLocalPositionHorl = dragLocalPosition;
    _updateDragGlobalPositionHorl = dragGlobalPosition;

    // Find empty slot
    Point<double>? pointFou;

    // final Point<double> pointT = pointOld + const Point<double>(0, -1);
    // final Point<double> pointB = pointOld + const Point<double>(0, 1);
    final Point<double> pointL = pointOld + const Point<double>(-1, 0);
    final Point<double> pointR = pointOld + const Point<double>(1, 0);

    String direction = "";

    // if (_listValidPoint.contains(pointT)) {
    //   if (!_listBookedPoint.contains(pointT)) {
    //     pointFou = pointT;
    //     direction = "T";
    //   }
    // }
    // if (_listValidPoint.contains(pointB)) {
    //   if (!_listBookedPoint.contains(pointB)) {
    //     pointFou = pointB;
    //     direction = "B";
    //   }
    // }

    if (_listValidPoint.contains(pointL)) {
      if (!_listBookedPoint.contains(pointL.toString())) {
        pointFou = pointL;
        direction = "L";
        print('found slot');
        print(pointFou);
      }
    }
    if (_listValidPoint.contains(pointR)) {
      if (!_listBookedPoint.contains(pointR.toString())) {
        pointFou = pointR;
        direction = "R";
        print('found slot');
        print(pointFou);
      }
    }

    final double dragX = dragLocalPosition.dx;

    // print('dragX: ' + dragX.toString());

    final double tmpInitialDragX = _tmpInitialDragX!;

    // print('tmpInitialDragX: ' + tmpInitialDragX.toString());

    final heightMax = widthMax;
    final double perpindahanX = dragX - tmpInitialDragX;
    final double homeBoxX = pointOld.x * heightMax;
    double newBoxX = homeBoxX + perpindahanX;

    // print('diffInnerX: ' + diffInnerX.toString());
    // print('newBoxX: ' + newBoxX.toString());

    if (null == pointFou) {
      double xMin = 0;
      double xMax = 0;
      xMin = (pointOld.x * widthMax) - 4;
      xMax = (pointOld.x * widthMax) + 4;

      if (newBoxX < xMin) {
        newBoxX = xMin;
      }
      if (newBoxX > xMax) {
        newBoxX = xMax;
      }

      final double newBoxY = pointOld.y * heightMax;
      final Point<double> uiPosition = Point(newBoxX, newBoxY);
      final point = Point(pointOld.x, pointOld.y);

      emit(KotakCubitStateMove(
        boxId: boxId,
        point: point,
        direction: direction,
        uiPosition: uiPosition,
        slowAnimate: false,
      ));
    }

    if (null != pointFou) {
      final heightMax = widthMax;

      double xMin = 0;
      double xMax = 0;
      final double xOld = pointOld.x * widthMax;
      final double xFou = pointFou.x * widthMax;

      if (xOld > xFou) {
        xMin = xFou;
        xMax = xOld;
      }
      if (xOld < xFou) {
        xMin = xOld;
        xMax = xFou;
      }
      if (xOld == xFou) {
        xMin = xOld;
        xMax = xOld;
      }

      xMin = xMin - 4;
      xMax = xMax + 4;

      if (newBoxX < xMin) {
        newBoxX = xMin;
      }
      if (newBoxX > xMax) {
        newBoxX = xMax;
      }

      final double newBoxY = pointOld.y * heightMax;

      final Point<double> uiPosition = Point(newBoxX, newBoxY);

      // print('move horizontal --------------------------------');

      pointFou = Point(pointOld.x, pointOld.y);

      emit(KotakCubitStateMove(
        boxId: boxId,
        point: pointFou,
        direction: direction,
        uiPosition: uiPosition,
        slowAnimate: false,
      ));
    }
  }

  Future<void> event_onHorizontalDragDown(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey key1,
    final Offset dragLocalPosition,
    final Offset dragGlobalPosition,
    final double widthMax,
  ) async {
    selectedDrag = 'h';

    _isNeedClosestAvailablePoint = true;
    _firstDragLocalPositionHorl = dragLocalPosition;
    _firstDragGlobalPositionHorl = dragGlobalPosition;
    _updateDragLocalPositionHorl = dragLocalPosition;
    _updateDragGlobalPositionHorl = dragGlobalPosition;

    final double dragX = dragLocalPosition.dx;
    final double dragY = dragLocalPosition.dy;
    final double boxX = convertGlobalKeyToX(key1);
    final double boxY = convertGlobalKeyToY(key1);

    print('dragX: ' + dragX.toString());
    print('dragY: ' + dragY.toString());
    print('boxX: ' + boxX.toString());
    print('boxY: ' + boxY.toString());

    _tmpInitialDragX = dragX;
    _tmpInitialDragY = dragY;

    print('tmpInitialDragX: ' + _tmpInitialDragX.toString());
    print('tmpInitialDragY: ' + _tmpInitialDragY.toString());
  }

  Future<void> event_onHorizontalDragStart(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey key1,
    final Offset dragLocalPosition,
    final Offset dragGlobalPosition,
    final double widthMax,
  ) async {
    _isNeedClosestAvailablePoint = true;
  }

  Future<void> event_onHorizontalDragCancel(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey key1,
    final double widthMax,
  ) async {
    selectedDrag = '';
  }

  Future<void> event_onHorizontalDragEnd(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey key1,
    final double widthMax,
  ) async {
    print('1');

    final dragLocalPosition = _updateDragLocalPositionHorl;
    final dragGlobalPosition = _updateDragGlobalPositionHorl;
    final firstDragLocalPosition = _firstDragLocalPositionHorl;
    final firstDragGlobalPosition = _firstDragGlobalPositionHorl;

    if (null == dragLocalPosition) {
      return;
    }
    if (null == dragGlobalPosition) {
      return;
    }

    if (null == firstDragLocalPosition) {
      return;
    }
    if (null == firstDragGlobalPosition) {
      return;
    }

    print('dragLocalPosition dx: ' + dragLocalPosition.dx.toString());
    print('dragGlobalPosition dx: ' + dragGlobalPosition.dx.toString());

    // Find empty slot
    Point<double>? pointFou;

    // final Point<double> pointT = pointOld + const Point<double>(0, -1);
    // final Point<double> pointB = pointOld + const Point<double>(0, 1);
    final Point<double> pointL = pointOld + const Point<double>(-1, 0);
    final Point<double> pointR = pointOld + const Point<double>(1, 0);

    String direction = "";

    // if (_listValidPoint.contains(pointT)) {
    //   if (!_listBookedPoint.contains(pointT)) {
    //     pointFou = pointT;
    //     direction = "T";
    //   }
    // }
    // if (_listValidPoint.contains(pointB)) {
    //   if (!_listBookedPoint.contains(pointB)) {
    //     pointFou = pointB;
    //     direction = "B";
    //   }
    // }

    if (_listValidPoint.contains(pointL)) {
      if (!_listBookedPoint.contains(pointL.toString())) {
        pointFou = pointL;
        direction = "L";
        print('found slot');
        print(pointFou);
      }
    }
    if (_listValidPoint.contains(pointR)) {
      if (!_listBookedPoint.contains(pointR.toString())) {
        pointFou = pointR;
        direction = "R";
        print('found slot');
        print(pointFou);
      }
    }

    final double dragX = dragLocalPosition.dx;

    print('dragX: ' + dragX.toString());

    final double tmpInitialDragX = _tmpInitialDragX!;

    // print('tmpInitialDragX: ' + tmpInitialDragX.toString());

    final heightMax = widthMax;
    final double perpindahanX = dragX - tmpInitialDragX;
    final double homeBoxX = pointOld.x * heightMax;
    double newBoxX = homeBoxX + perpindahanX;

    // print('diffInnerX: ' + diffInnerX.toString());
    // print('newBoxX: ' + newBoxX.toString());

    if (null == pointFou) {
      print('no slot found. go back to home.');
      try {
        final KotakCubitStateMove tmpState = this.state as KotakCubitStateMove;
        var uiPosition = convertPointToUiPosition(tmpState.point, widthMax);
        emit(KotakCubitStateMove(
          boxId: tmpState.boxId,
          point: tmpState.point,
          direction: tmpState.direction,
          uiPosition: uiPosition,
          slowAnimate: true,
        ));
      } catch (e) {
        //
      }
      return;
    }

    if (null != pointFou) {
      print('2');

      if (direction == 'L' && (tmpInitialDragX > dragX)) {
        // ok
      } else {
        if (direction == 'R' && (tmpInitialDragX < dragX)) {
          // ok
        } else {
          print('skipped. wrong direction.');
          try {
            final KotakCubitStateMove tmpState =
                this.state as KotakCubitStateMove;
            var uiPosition = convertPointToUiPosition(tmpState.point, widthMax);
            emit(KotakCubitStateMove(
              boxId: tmpState.boxId,
              point: tmpState.point,
              direction: tmpState.direction,
              uiPosition: uiPosition,
              slowAnimate: true,
            ));
          } catch (e) {
            //
          }
          return;
        }
      }

      double xMin = 0;
      double xMax = 0;
      final double xOld = pointOld.x * widthMax;
      final double xFou = pointFou.x * widthMax;

      if (xOld > xFou) {
        xMin = xFou;
        xMax = xOld;
      }
      if (xOld < xFou) {
        xMin = xOld;
        xMax = xFou;
      }
      if (xOld == xFou) {
        xMin = xOld;
        xMax = xOld;
      }

      if (newBoxX < xMin) {
        newBoxX = xMin;
      }
      if (newBoxX > xMax) {
        newBoxX = xMax;
      }

      if (!_isNeedClosestAvailablePoint) {
        print('3');
        return;
      }

      final heightMax = widthMax;
      final double newBoxY = pointFou.y * heightMax;

      /// Cancel moving effect
      final double setengahSelisihX = widthMax / 2;

      print('setengahSelisihX: ' + setengahSelisihX.toString());

      ///  5 = 5 - 0
      /// -5 = 0 - 5
      double perpindahanX = dragLocalPosition.dx - firstDragLocalPosition.dx;
      if (perpindahanX < 0) {
        perpindahanX = perpindahanX * -1;
      }

      print('perpindahanX: ' + perpindahanX.toString());

      if (perpindahanX > setengahSelisihX) {
        // move to new location
        newBoxX = pointFou.x * widthMax;

        pointFou = pointFou;
      } else {
        // do not move
        newBoxX = pointOld.x * widthMax;

        pointFou = Point(pointOld.x, pointOld.y);
      }

      final Point<double> uiPosition = Point(newBoxX, newBoxY);

      print('move--------------------------------');

      _listBookedPoint.remove(pointOld.toString());
      _listBookedPoint.add(pointFou.toString());
      _lastRemovedPoint = Point(pointOld.x, pointOld.y);

      _mappedPointAndId[pointOld.toString()] = null;
      _mappedPointAndId[pointFou.toString()] = boxId;

      if (pointOld != pointFou) {
        _onTileWillMovedToNewPoint();
      }

      emit(KotakCubitStateMove(
        boxId: boxId,
        point: pointFou,
        direction: direction,
        uiPosition: uiPosition,
        slowAnimate: true,
      ));
    }
  }

  /// Tap -------------------------------------------------------------------------

  Future<void> event_onTapDown(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey<State<StatefulWidget>> key1,
    final Offset tapLocalPosition,
    final Offset tapGlobalPosition,
    final double widthMax,
  ) async {
    final double tapX = tapLocalPosition.dx;
    final double tapY = tapLocalPosition.dy;
    final double boxX = convertGlobalKeyToX(key1);
    final double boxY = convertGlobalKeyToY(key1);

    print('tapX: ' + tapX.toString());
    print('tapY: ' + tapY.toString());
    print('boxX: ' + boxX.toString());
    print('boxY: ' + boxY.toString());

    // _tmpInitialBoxX = boxX;
    // _tmpInitialBoxY = boxY;

    // print('tmpInitialBoxX: ' + _tmpInitialBoxX.toString());
    // print('tmpInitialBoxY: ' + _tmpInitialBoxY.toString());
  }

  Future<void> event_onTapCancel(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey<State<StatefulWidget>> key1,
    final double widthMax,
  ) async {
    _isNeedClosestAvailablePoint = true;
  }

  Future<void> event_onTapUp(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey<State<StatefulWidget>> key1,
    final Offset tapLocalPosition,
    final Offset tapGlobalPosition,
    final double widthMax,
  ) async {
    _isNeedClosestAvailablePoint = false; // TODO: ???
  }

  /// Vertical Drag -------------------------------------------------------------------------

  Future<void> event_onVerticalDragDown(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey key1,
    final Offset dragLocalPosition,
    final Offset dragGlobalPosition,
    final double widthMax,
  ) async {
    selectedDrag = 'v';

    _isNeedClosestAvailablePoint = true;
    _firstDragLocalPositionVerl = dragLocalPosition;
    _firstDragGlobalPositionVerl = dragGlobalPosition;
    _updateDragLocalPositionVerl = dragLocalPosition;
    _updateDragGlobalPositionVerl = dragGlobalPosition;

    final double dragX = dragLocalPosition.dx;
    final double dragY = dragLocalPosition.dy;
    final double boxX = convertGlobalKeyToX(key1);
    final double boxY = convertGlobalKeyToY(key1);

    print('dragX: ' + dragX.toString());
    print('dragY: ' + dragY.toString());
    print('boxX: ' + boxX.toString());
    print('boxY: ' + boxY.toString());

    _tmpInitialDragX = dragX;
    _tmpInitialDragY = dragY;

    print('tmpInitialDragX: ' + _tmpInitialDragX.toString());
    print('tmpInitialDragY: ' + _tmpInitialDragY.toString());
  }

  Future<void> event_onVerticalDragStart(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey key1,
    final Offset dragLocalPosition,
    final Offset dragGlobalPosition,
    final double widthMax,
  ) async {
    selectedDrag = 'v';

    _isNeedClosestAvailablePoint = true;
  }

  Future<void> event_onVerticalDragCancel(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey key1,
    final double widthMax,
  ) async {
    //
  }

  Future<void> event_onVerticalDragUpdate(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey key1,
    final Offset dragLocalPosition,
    final Offset dragGlobalPosition,
    final double widthMax,
  ) async {
    _isNeedClosestAvailablePoint = true;
    _updateDragLocalPositionVerl = dragLocalPosition;
    _updateDragGlobalPositionVerl = dragGlobalPosition;

    // Find empty slot
    Point<double>? pointFou;

    final Point<double> pointT = pointOld + const Point<double>(0, -1);
    final Point<double> pointB = pointOld + const Point<double>(0, 1);
    // final Point<double> pointL = pointOld + const Point<double>(-1, 0);
    // final Point<double> pointR = pointOld + const Point<double>(1, 0);

    String direction = "";

    if (_listValidPoint.contains(pointT)) {
      if (!_listBookedPoint.contains(pointT.toString())) {
        pointFou = pointT;
        direction = "T";
        print('found slot');
        print(pointFou);
      }
    }
    if (_listValidPoint.contains(pointB)) {
      if (!_listBookedPoint.contains(pointB.toString())) {
        pointFou = pointB;
        direction = "B";
        print('found slot');
        print(pointFou);
      }
    }

    // if (_listValidPoint.contains(pointL)) {
    //   if (!_listBookedPoint.contains(pointL)) {
    //     pointFou = pointL;
    //     direction = "L";
    //   }
    // }
    // if (_listValidPoint.contains(pointR)) {
    //   if (!_listBookedPoint.contains(pointR)) {
    //     pointFou = pointR;
    //     direction = "R";
    //   }
    // }

    final double dragY = dragLocalPosition.dy;

    // print('dragY: ' + dragY.toString());

    final double tmpInitialDragY = _tmpInitialDragY!;

    // print('tmpInitialDragY: ' + tmpInitialDragY.toString());

    final heightMax = widthMax;
    final double perpindahanY = dragY - tmpInitialDragY;
    final double homeBoxY = pointOld.y * heightMax;
    double newBoxY = homeBoxY + perpindahanY;

    if (null == pointFou) {
      double yMin = 0;
      double yMax = 0;
      yMin = (pointOld.y * heightMax) - 4;
      yMax = (pointOld.y * heightMax) + 4;

      if (newBoxY < yMin) {
        newBoxY = yMin;
      }
      if (newBoxY > yMax) {
        newBoxY = yMax;
      }

      final double newBoxX = pointOld.x * widthMax;
      final Point<double> uiPosition = Point(newBoxX, newBoxY);
      final point = Point(pointOld.x, pointOld.y);

      emit(KotakCubitStateMove(
        boxId: boxId,
        point: point,
        direction: direction,
        uiPosition: uiPosition,
        slowAnimate: false,
      ));
    }

    if (null != pointFou) {
      double yMin = 0;
      double yMax = 0;
      final double yOld = pointOld.y * heightMax;
      final double yFou = pointFou.y * heightMax;

      if (yOld > yFou) {
        yMin = yFou;
        yMax = yOld;
      }
      if (yOld < yFou) {
        yMin = yOld;
        yMax = yFou;
      }
      if (yOld == yFou) {
        yMin = yOld;
        yMax = yOld;
      }

      yMin = yMin - 4;
      yMax = yMax + 4;

      if (newBoxY < yMin) {
        newBoxY = yMin;
      }
      if (newBoxY > yMax) {
        newBoxY = yMax;
      }

      double newBoxX = pointOld.x * widthMax;

      final Point<double> uiPosition = Point(newBoxX, newBoxY);

      // print('move vertical --------------------------------');

      pointFou = Point(pointOld.x, pointOld.y);

      // print(uiPosition);

      emit(KotakCubitStateMove(
        boxId: boxId,
        point: pointFou,
        direction: direction,
        uiPosition: uiPosition,
        slowAnimate: false,
      ));
    }
  }

  Future<void> event_onVerticalDragEnd(
    final String boxId,
    final Point<double> pointOld,
    final GlobalKey key1,
    final double widthMax,
  ) async {
    print('1');

    if (selectedDrag != 'v') {
      print('event_onVerticalDragEnd: selectedDrag: ' + selectedDrag);
      return;
    }

    final dragLocalPosition = _updateDragLocalPositionVerl;
    final dragGlobalPosition = _updateDragGlobalPositionVerl;
    final firstDragLocalPosition = _firstDragLocalPositionVerl;
    final firstDragGlobalPosition = _firstDragGlobalPositionVerl;

    if (null == dragLocalPosition) {
      return;
    }
    if (null == dragGlobalPosition) {
      return;
    }

    if (null == firstDragLocalPosition) {
      return;
    }
    if (null == firstDragGlobalPosition) {
      return;
    }

    print('dragLocalPosition dy: ' + dragLocalPosition.dy.toString());
    print('dragGlobalPosition dy: ' + dragGlobalPosition.dy.toString());

    // Find empty slot
    Point<double>? pointFou;

    final Point<double> pointT = pointOld + const Point<double>(0, -1);
    final Point<double> pointB = pointOld + const Point<double>(0, 1);
    // final Point<double> pointL = pointOld + const Point<double>(-1, 0);
    // final Point<double> pointR = pointOld + const Point<double>(1, 0);

    String direction = "";

    if (_listValidPoint.contains(pointT)) {
      if (!_listBookedPoint.contains(pointT.toString())) {
        pointFou = pointT;
        direction = "T";
      }
    }
    if (_listValidPoint.contains(pointB)) {
      if (!_listBookedPoint.contains(pointB.toString())) {
        pointFou = pointB;
        direction = "B";
      }
    }

    // if (_listValidPoint.contains(pointL)) {
    //   if (!_listBookedPoint.contains(pointL)) {
    //     pointFou = pointL;
    //     direction = "L";
    //   }
    // }
    // if (_listValidPoint.contains(pointR)) {
    //   if (!_listBookedPoint.contains(pointR)) {
    //     pointFou = pointR;
    //     direction = "R";
    //   }
    // }

    final double dragY = dragLocalPosition.dy;

    print('dragY: ' + dragY.toString());

    final double tmpInitialDragY = _tmpInitialDragY!;

    print('tmpInitialDragY: ' + tmpInitialDragY.toString());

    final heightMax = widthMax;
    final double perpindahanY = dragY - tmpInitialDragY;
    final double homeBoxY = pointOld.y * heightMax;
    double newBoxY = homeBoxY + perpindahanY;

    print('heightMax: ' + heightMax.toString());
    print('perpindahanY: ' + perpindahanY.toString());
    print('homeBoxY: ' + homeBoxY.toString());
    print('newBoxY: ' + newBoxY.toString());

    if (null == pointFou) {
      print('no slot found. go back to home.');
      try {
        final KotakCubitStateMove tmpState = this.state as KotakCubitStateMove;
        var uiPosition = convertPointToUiPosition(tmpState.point, widthMax);
        emit(KotakCubitStateMove(
          boxId: tmpState.boxId,
          point: tmpState.point,
          direction: tmpState.direction,
          uiPosition: uiPosition,
          slowAnimate: true,
        ));
      } catch (e) {
        //
      }
      return;
    }

    if (null != pointFou) {
      print('2');

      if (direction == 'T' && (tmpInitialDragY > dragY)) {
        // ok
      } else {
        if (direction == 'B' && (tmpInitialDragY < dragY)) {
          // ok
        } else {
          print('skipped. wrong direction.');
          try {
            final KotakCubitStateMove tmpState =
                this.state as KotakCubitStateMove;
            var uiPosition = convertPointToUiPosition(tmpState.point, widthMax);
            emit(KotakCubitStateMove(
              boxId: tmpState.boxId,
              point: tmpState.point,
              direction: tmpState.direction,
              uiPosition: uiPosition,
              slowAnimate: true,
            ));
          } catch (e) {
            //
          }
          return;
        }
      }

      double yMin = 0;
      double yMax = 0;
      final double yOld = pointOld.y * heightMax;
      final double yFou = pointFou.y * heightMax;

      if (yOld > yFou) {
        yMin = yFou;
        yMax = yOld;
      }
      if (yOld < yFou) {
        yMin = yOld;
        yMax = yFou;
      }
      if (yOld == yFou) {
        yMin = yOld;
        yMax = yOld;
      }

      if (newBoxY < yMin) {
        newBoxY = yMin;
      }
      if (newBoxY > yMax) {
        newBoxY = yMax;
      }

      if (!_isNeedClosestAvailablePoint) {
        print('3');
        return;
      }

      final double newBoxX = pointOld.x * widthMax;

      /// Cancel moving effect
      final double setengahSelisihY = heightMax / 2;

      print('setengahSelisihY: ' + setengahSelisihY.toString());

      ///  5 = 5 - 0
      /// -5 = 0 - 5
      double perpindahanY = dragY - firstDragLocalPosition.dy;

      if (perpindahanY < 0) {
        perpindahanY = perpindahanY * -1;
      }

      print('perpindahanY: ' + perpindahanY.toString());

      if (perpindahanY > setengahSelisihY) {
        // move to new location
        newBoxY = pointFou.y * heightMax;

        pointFou = pointFou;
      } else {
        // do not move
        newBoxY = pointOld.y * heightMax;

        pointFou = Point(pointOld.x, pointOld.y);
      }

      final Point<double> uiPosition = Point(newBoxX, newBoxY);

      print('end vertical --------------------------------');

      _listBookedPoint.remove(pointOld.toString());
      _listBookedPoint.add(pointFou.toString());
      _lastRemovedPoint = Point(pointOld.x, pointOld.y);

      _mappedPointAndId[pointOld.toString()] = null;
      _mappedPointAndId[pointFou.toString()] = boxId;

      if (pointOld != pointFou) {
        _onTileWillMovedToNewPoint();
      }

      emit(KotakCubitStateMove(
        boxId: boxId,
        point: pointFou,
        direction: direction,
        uiPosition: uiPosition,
        slowAnimate: true,
      ));
    }
  }

  Point<double> convertPointToUiPosition(Point<double> point, double widthMax) {
    var x = point.x * widthMax;
    var y = point.y * widthMax;
    return Point(x, y);
  }

  Future<void> btnShuffleTiles() async {
    if (_stopwatch.isRunning) {
      // TODO: ???
      return;
    }

    _isShuffling = true;
    _isStopwatchEnabled = false;
    _shuffleCounterTick = 0;

    _stopwatch.stop();
    _stopwatch.reset();
    emit(_sttStopwatch = KotakCubitStateStopwatch(text: '00:00:00'));

    _isInPracticeMode = true;
    emit(_sttPracticeMode = KotakCubitStatePracticeMode(text: 'Shuffling'));

    Timer.periodic(const Duration(milliseconds: 50), (timer) {
      _timerShuffle = timer;

      print('tick: ' + timer.tick.toString());

      print('_lastRemovedPoint: ' + _lastRemovedPoint.toString());
      _randomTap();

      _shuffleCounterTick++;
      if (_shuffleCounterTick >= configShuffleMoveCount) {
        timer.cancel();

        _isStopwatchEnabled = true;

        _moveCount = 0;
        emit(_sttMoveCount =
            KotakCubitStateCountMove(text: _moveCount.toString()));

        _isInPracticeMode = false;
        emit(_sttPracticeMode = KotakCubitStatePracticeMode(text: 'Ready'));

        emit(_sttStopwatch = KotakCubitStateStopwatch(text: '00:00:00'));

        _isShuffling = false;
      }
    });
  }

  List<Point<double>> listPoint() {
    return _listPoint;
  }

  void _setupInitListPoint() {
    _listPoint.clear();

    _listPoint.add(Point(0, 0));
    _listPoint.add(Point(1, 0));
    _listPoint.add(Point(2, 0));
    _listPoint.add(Point(3, 0));

    _listPoint.add(Point(0, 1));
    _listPoint.add(Point(1, 1));
    _listPoint.add(Point(2, 1));
    _listPoint.add(Point(3, 1));

    _listPoint.add(Point(0, 2));
    _listPoint.add(Point(1, 2));
    _listPoint.add(Point(2, 2));
    _listPoint.add(Point(3, 2));

    _listPoint.add(Point(0, 3));
    _listPoint.add(Point(1, 3));
    _listPoint.add(Point(2, 3));
    // _listPoint.add(Point(3, 3));
  }

  void _setupInitMappedPointAndId() {
    _listPoint.asMap().forEach((index, Point<double> point) {
      _mappedPointAndId[point.toString()] = 'title.' + index.toString();
    });
  }

  Future<void> event_initStateKotak(double configTileMaxWidth) async {
    _configTileMaxWidth ??= configTileMaxWidth;
  }

  void _randomTap() {
    print('_randomTap------------------------------------');
    final configTileMaxWidth = _configTileMaxWidth;
    if (null == configTileMaxWidth) {
      return;
    }

    final pointEmptySlot = Point(_lastRemovedPoint.x, _lastRemovedPoint.y);
    print('pointEmptySlot: ' + pointEmptySlot.toString());

    final Point<double> pointT = pointEmptySlot + const Point<double>(0, -1);
    final Point<double> pointB = pointEmptySlot + const Point<double>(0, 1);
    final Point<double> pointL = pointEmptySlot + const Point<double>(-1, 0);
    final Point<double> pointR = pointEmptySlot + const Point<double>(1, 0);
    final listPointLRTB = [pointL, pointR, pointT, pointB];

    List<Point<double>> listPosiblePoint = [];
    _listPoint.asMap().forEach((int boxIndex, Point<double> boxPoint) {
      listPointLRTB.asMap().forEach((int iLRTB, Point<double> pointLRTB) {
        if (boxPoint == pointLRTB) {
          listPosiblePoint.add(Point(boxPoint.x, boxPoint.y));
        }
      });
    });

    print('listPosiblePoint:' + listPosiblePoint.toString());
    listPosiblePoint.shuffle();
    listPosiblePoint.shuffle();
    listPosiblePoint.shuffle();
    Point<double> pointToTap = listPosiblePoint[0];
    try {
      final String? boxId = _mappedPointAndId[pointToTap.toString()];
      if (null != boxId) {
        print('boxId: ' + boxId);
        print('pointToTap: ' + pointToTap.toString());
        var point1 = Point(pointToTap.x, pointToTap.y);
        event_onTap(boxId, point1, configTileMaxWidth);
      }
    } catch (e) {
      print('error: ' + e.toString());
    }
  }

  Future<void> btnReset() async {
    _timerShuffle?.cancel();
    _timerStartStopwatch?.cancel();

    _isInPracticeMode = true;
    emit(_sttPracticeMode = KotakCubitStatePracticeMode(text: 'Practice'));

    _isStopwatchEnabled = false;

    _stopwatch.stop();
    _stopwatch.reset();
    emit(_sttStopwatch = KotakCubitStateStopwatch(text: '00:00:00'));

    _moveCount = 0;
    emit(_sttMoveCount = KotakCubitStateCountMove(text: _moveCount.toString()));

    _init();

    _listPoint.asMap().forEach((index, point) {
      final String boxId = 'title.' + index.toString();
      final pointFou = Point(point.x, point.y);
      final uiPosition =
          convertPointToUiPosition(pointFou, _configTileMaxWidth!);
      emit(KotakCubitStateMove(
        boxId: boxId,
        point: pointFou,
        direction: 'T',
        uiPosition: uiPosition,
        slowAnimate: true,
      ));
    });
  }

  void _init() {
    _lastRemovedPoint = const Point(3, 3);
    // _configTileMaxWidth = null; // Keep OLD value, dont reset.

    _listPoint.clear();
    _listBookedPoint.clear();
    _mappedPointAndId.clear();
    _listValidPoint.clear();

    _tmpInitialDragX = null;
    _tmpInitialDragY = null;

    _isNeedClosestAvailablePoint = false;

    _updateDragLocalPositionHorl = null;
    _updateDragGlobalPositionHorl = null;
    _firstDragLocalPositionHorl = null;
    _firstDragGlobalPositionHorl = null;

    _updateDragLocalPositionVerl = null;
    _updateDragGlobalPositionVerl = null;
    _firstDragLocalPositionVerl = null;
    _firstDragGlobalPositionVerl = null;

    String selectedDrag = '';

    int _stopwatchCounterTick = 0;

    ///

    _setupInitListBookedPoint();
    _setupInitListValidPoint();
    _setupInitListCorrectIdPoint();
    _setupInitListPoint();
    _setupInitMappedPointAndId();
  }

  void _setupInitListBookedPoint() {
    _listBookedPoint.clear();

    /// List Box Point  (x, y)
    _listBookedPoint.add(const Point<double>(0, 0).toString());
    _listBookedPoint.add(const Point<double>(1, 0).toString());
    _listBookedPoint.add(const Point<double>(2, 0).toString());
    _listBookedPoint.add(const Point<double>(3, 0).toString());

    _listBookedPoint.add(const Point<double>(0, 1).toString());
    _listBookedPoint.add(const Point<double>(1, 1).toString());
    _listBookedPoint.add(const Point<double>(2, 1).toString());
    _listBookedPoint.add(const Point<double>(3, 1).toString());

    _listBookedPoint.add(const Point<double>(0, 2).toString());
    _listBookedPoint.add(const Point<double>(1, 2).toString());
    _listBookedPoint.add(const Point<double>(2, 2).toString());
    _listBookedPoint.add(const Point<double>(3, 2).toString());

    _listBookedPoint.add(const Point<double>(0, 3).toString());
    _listBookedPoint.add(const Point<double>(1, 3).toString());
    _listBookedPoint.add(const Point<double>(2, 3).toString());
    // _listBookedPoint.add(Point(3, 3).toString()); // Nope
  }

  void _setupInitListValidPoint() {
    _listValidPoint.clear();

    _listValidPoint.add(const Point<double>(0, 0));
    _listValidPoint.add(const Point<double>(1, 0));
    _listValidPoint.add(const Point<double>(2, 0));
    _listValidPoint.add(const Point<double>(3, 0));

    _listValidPoint.add(const Point<double>(0, 1));
    _listValidPoint.add(const Point<double>(1, 1));
    _listValidPoint.add(const Point<double>(2, 1));
    _listValidPoint.add(const Point<double>(3, 1));

    _listValidPoint.add(const Point<double>(0, 2));
    _listValidPoint.add(const Point<double>(1, 2));
    _listValidPoint.add(const Point<double>(2, 2));
    _listValidPoint.add(const Point<double>(3, 2));

    _listValidPoint.add(const Point<double>(0, 3));
    _listValidPoint.add(const Point<double>(1, 3));
    _listValidPoint.add(const Point<double>(2, 3));
    _listValidPoint.add(const Point<double>(3, 3));
  }

  void _setupInitListCorrectIdPoint() {
    _mappedPointAndId.clear();
    _mappedCorrectIdPoint.clear();

    _listValidPoint.asMap().forEach((index, point) {
      if (point == const Point<double>(3, 3)) {
        _mappedPointAndId[point.toString()] = null;
      } else {
        final boxId = 'title.' + index.toString();

        _mappedPointAndId[point.toString()] = boxId;
        _mappedCorrectIdPoint[boxId] = point.toString();
      }
    });
  }

  bool isOnCorrectSlot(String boxId, Point<double> checkPoint) {
    if (!_listValidPoint.contains(checkPoint)) {
      return false;
    }

    if (!_mappedCorrectIdPoint.containsKey(boxId)) {
      return false;
    }

    final strCorrectPoint = _mappedCorrectIdPoint[boxId];
    return checkPoint.toString() == strCorrectPoint;
  }

  String getLastStopwatch() {
    return _sttStopwatch.text;
  }

  String getLastPracticeMode() {
    final mode = _sttPracticeMode;
    if (null != mode) {
      return mode.text;
    }

    if (_isInPracticeMode) {
      return "Practice";
    } else {
      return "Playing";
    }
  }

  String getLastCountMove() {
    return _sttMoveCount.text;
  }

  Future<void> btnStart() async {
    _timerStartStopwatch?.cancel();

    _stopwatch.stop();
    _stopwatch.reset();
    _stopwatch.start();

    Timer.periodic(const Duration(milliseconds: 500), (timer) {
      _timerStartStopwatch = timer;

      final int mili = _stopwatch.elapsedMilliseconds;
      final duration = Duration(milliseconds: mili);
      String strDuration = duration.toString().split('.').first.padLeft(8, "0");
      print(strDuration);

      emit(_sttStopwatch = KotakCubitStateStopwatch(text: strDuration));
    });
  }

  void _onTileWillMovedToNewPoint() {
    if (!_isInPracticeMode) {
      if (0 == _moveCount) {
        if (_isStopwatchEnabled) {
          if (!_isShuffling) {
            emit(_sttPracticeMode =
                KotakCubitStatePracticeMode(text: 'Playing'));
            btnStart();
          }
        }
      }

      _moveCount++;
      emit(_sttMoveCount =
          KotakCubitStateCountMove(text: _moveCount.toString()));
    }

    if (!_isInPracticeMode) {
      if (!_isShuffling) {
        if (!_isStopwatchEnabled) {
          asyncCheckSolved();
        }
      }
    }

    if (!_isShuffling) {
      asyncCheckWatchTapping();
    }
  }

  Future<void> asyncCheckSolved() async {
    var hash = HashMap<String, String>();

    _listValidPoint.asMap().forEach((i, point) {
      if (point != const Point(3, 3)) {
        final String boxId = 'title.' + i.toString();
        final pointCorrect = _mappedCorrectIdPoint[boxId];

        _mappedPointAndId.forEach((checkPoint, checkBoxId) {
          if (checkPoint == pointCorrect) {
            if (checkBoxId == boxId) {
              hash[boxId] = pointCorrect.toString();
            }
          }
        });
      }
    });

    if (hash.length == _mappedCorrectIdPoint.length) {
      print('--------------SOLVED--------------');

      final time = _sttStopwatch.text;
      final move = _sttMoveCount.text;
      emit(KotakCubitStateSolved(
        textTotalTime: time,
        textTotalMove: move,
      ));
    }
  }

  Future<void> asyncCheckWatchTapping() async {
    _timerTapWatcher?.cancel();

    if (_isInPracticeMode) {
      emit(_sttPracticeMode = KotakCubitStatePracticeMode(text: 'Trying'));
    } else {
      emit(_sttPracticeMode = KotakCubitStatePracticeMode(text: 'Moving'));
    }

    final timer = Timer(const Duration(milliseconds: 4000), () {
      if (_isInPracticeMode) {
        emit(_sttPracticeMode = KotakCubitStatePracticeMode(text: 'Idle'));
      } else {
        emit(_sttPracticeMode = KotakCubitStatePracticeMode(text: 'Thinking'));
      }
    });
    _timerTapWatcher = timer;
  }
}
