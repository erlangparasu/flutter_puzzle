// ignore_for_file: avoid_print

import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

double convertGlobalKeyToX(final GlobalKey key1) {
  final RenderBox box = key1.currentContext!.findRenderObject() as RenderBox;
  final Offset position =
      box.localToGlobal(Offset.zero); // this is global position
  final double x = position.dx;
  return x;
}

double convertGlobalKeyToY(final GlobalKey key1) {
  final RenderBox box = key1.currentContext!.findRenderObject() as RenderBox;
  final Offset position =
      box.localToGlobal(Offset.zero); // this is global position
  final double y = position.dy;
  return y;
}
